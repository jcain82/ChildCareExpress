SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';



DROP SCHEMA IF EXISTS `ChildCareExpress` ;
CREATE SCHEMA IF NOT EXISTS `ChildCareExpress` DEFAULT CHARACTER SET utf8 ;
USE `ChildCareExpress` ;



-- -----------------------------------------------------

-- Table `ChildCareExpress`.`System_Config`

-- -----------------------------------------------------

DROP TABLE IF EXISTS `ChildCareExpress`.`System_Config` ;



CREATE  TABLE IF NOT EXISTS `ChildCareExpress`.`System_Config` (

  `id` INT NOT NULL ,

  `Facility` VARCHAR(20) NOT NULL DEFAULT 'Child Care Express' ,

  `AddrID` INT NOT NULL DEFAULT 1 ,

  PRIMARY KEY (`id`) )

ENGINE = InnoDB;



-- -----------------------------------------------------

-- Table `ChildCareExpress`.`System_Groups`

-- -----------------------------------------------------

DROP TABLE IF EXISTS `ChildCareExpress`.`System_Groups` ;



CREATE  TABLE IF NOT EXISTS `ChildCareExpress`.`System_Groups` (

  `id` INT NOT NULL ,

  `GroupName` VARCHAR(45) NOT NULL ,

  `GroupRights` VARCHAR(45) NULL ,

  PRIMARY KEY (`id`) )

ENGINE = InnoDB;




-- -----------------------------------------------------

-- Table `ChildCareExpress`.`System_Users`

-- -----------------------------------------------------

DROP TABLE IF EXISTS `ChildCareExpress`.`System_Users` ;



CREATE  TABLE IF NOT EXISTS `ChildCareExpress`.`System_Users` (

  `id` INT NOT NULL ,

  `UserID` VARCHAR(25) NOT NULL ,

  `Password` VARCHAR(25) NOT NULL ,
  
  `PersonID` INT NOT NULL ,

  `GroupID` INT NULL ,

  PRIMARY KEY (`id`))

ENGINE = InnoDB;



-- -----------------------------------------------------

-- Table `ChildCareExpress`.`ChildToPerson`

-- -----------------------------------------------------

DROP TABLE IF EXISTS `ChildCareExpress`.`ChildToPerson` ;



CREATE  TABLE IF NOT EXISTS `ChildCareExpress`.`ChildToPerson` (

  `id` INT NOT NULL ,

  `ChildID` INT NOT NULL ,

  `PersonID` INT NOT NULL ,

  `Ptype` VARCHAR(1) NOT NULL ,

  PRIMARY KEY (`id`) )

ENGINE = InnoDB;





-- -----------------------------------------------------

-- Table `ChildCareExpress`.`Child`

-- -----------------------------------------------------

DROP TABLE IF EXISTS `ChildCareExpress`.`Child` ;



CREATE  TABLE IF NOT EXISTS `ChildCareExpress`.`Child` (

  `id` INT NOT NULL ,

  `Firstname` VARCHAR(20) NOT NULL ,

  `Lastname` VARCHAR(25) NOT NULL ,

  `SSN` VARCHAR(11) NOT NULL ,

  `Sex` VARCHAR(1) NOT NULL ,

  `Enrolled` DATE NOT NULL ,

  `Transport` VARCHAR(1) NOT NULL ,

  PRIMARY KEY (`id`) )

ENGINE = InnoDB;





-- -----------------------------------------------------

-- Table `ChildCareExpress`.`Cities`

-- -----------------------------------------------------

DROP TABLE IF EXISTS `ChildCareExpress`.`Cities` ;






-- -----------------------------------------------------

-- Table `ChildCareExpress`.`Zipcodes`

-- -----------------------------------------------------

DROP TABLE IF EXISTS `ChildCareExpress`.`Zipcodes` ;






-- -----------------------------------------------------

-- Table `ChildCareExpress`.`States`

-- -----------------------------------------------------

DROP TABLE IF EXISTS `ChildCareExpress`.`States` ;





-- -----------------------------------------------------

-- Table `ChildCareExpress`.`Addresses`

-- -----------------------------------------------------

DROP TABLE IF EXISTS `ChildCareExpress`.`Addresses` ;



CREATE  TABLE IF NOT EXISTS `ChildCareExpress`.`Addresses` (

  `id` INT NOT NULL ,

  `Address1` VARCHAR(75) NOT NULL ,

  `Address2` VARCHAR(75) NULL ,

  `Email` VARCHAR(45) NULL ,

  `Cities` VARCHAR(20) NOT NULL ,

  `Zipcodes` VARCHAR(5) NOT NULL ,

  `States` VARCHAR(2) NOT NULL ,
  
  `Hphone` VARCHAR(16) NULL ,
  
  `Cphone` VARCHAR(16) NULL ,
  
  `Wphone` VARCHAR(16) NULL ,

  PRIMARY KEY (`id`) )

ENGINE = InnoDB;




-- -----------------------------------------------------

-- Table `ChildCareExpress`.`Person`

-- -----------------------------------------------------

DROP TABLE IF EXISTS `ChildCareExpress`.`Person` ;



CREATE  TABLE IF NOT EXISTS `ChildCareExpress`.`Person` (

  `id` INT NOT NULL ,

  `Firstname` VARCHAR(20) NOT NULL ,

  `Lastname` VARCHAR(25) NOT NULL ,

  `SSN` VARCHAR(11) NOT NULL ,

  `Driverslic` VARCHAR(15) NULL ,

  `MakesPayment` VARCHAR(2) NOT NULL ,

  `AddrID` INT NOT NULL ,

  `EmployerID` INT NULL ,

  PRIMARY KEY (`id`) )

ENGINE = InnoDB;




-- -----------------------------------------------------

-- Table `ChildCareExpress`.`Employer`

-- -----------------------------------------------------

DROP TABLE IF EXISTS `ChildCareExpress`.`Employer` ;



CREATE  TABLE IF NOT EXISTS `ChildCareExpress`.`Employer` (

  `id` INT NOT NULL ,

  `Name` VARCHAR(45) NOT NULL ,

  `AddrID` INT NOT NULL ,

  PRIMARY KEY (`id`) )

ENGINE = InnoDB;





-- -----------------------------------------------------

-- Table `ChildCareExpress`.`MedicalContacts`

-- -----------------------------------------------------

DROP TABLE IF EXISTS `ChildCareExpress`.`MedicalContacts` ;



CREATE  TABLE IF NOT EXISTS `ChildCareExpress`.`MedicalContacts` (

  `Childid` INT NOT NULL ,

  `Name` VARCHAR(45) NOT NULL ,

  `Wphone` VARCHAR(16) NOT NULL ,

  `Mtype` VARCHAR(1) NULL ,

  `AddrID` INT NOT NULL 
)

ENGINE = InnoDB;





-- -----------------------------------------------------

-- Table `ChildCareExpress`.`Phonenums`

-- -----------------------------------------------------

DROP TABLE IF EXISTS `ChildCareExpress`.`Phonenums` ;




-- -----------------------------------------------------

-- Table `ChildCareExpress`.`Allergies`

-- -----------------------------------------------------

DROP TABLE IF EXISTS `ChildCareExpress`.`Allergies` ;



CREATE  TABLE IF NOT EXISTS `ChildCareExpress`.`Allergies` (

  `id` INT NOT NULL ,

  `Allergy` VARCHAR(1) NOT NULL ,

  `Notes` VARCHAR(255) NOT NULL )

ENGINE = InnoDB;





-- -----------------------------------------------------

-- Table `ChildCareExpress`.`MedicalCondition`

-- -----------------------------------------------------

DROP TABLE IF EXISTS `ChildCareExpress`.`MedicalCondition` ;



CREATE  TABLE IF NOT EXISTS `ChildCareExpress`.`MedicalCondition` (

  `id` INT NOT NULL ,

  `Condition` VARCHAR(20) NOT NULL ,

  `Notes` VARCHAR(255) NOT NULL )

ENGINE = InnoDB;







-- CREATE USER `cce` IDENTIFIED BY 'Password1';





SET SQL_MODE=@OLD_SQL_MODE;

SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;



-- -----------------------------------------------------

-- Data for table `ChildCareExpress`.`System_Users`

-- -----------------------------------------------------

SET AUTOCOMMIT=0;

USE `ChildCareExpress`;
INSERT INTO `ChildCareExpress`.`Child` (`id`, `Firstname`, `Lastname`, `SSN`, `Sex`, `Enrolled`, `Transport`) VALUES ('0', 'John', 'Doe', '000-00-0000', 'M', '2010/10/20', 'Y');
INSERT INTO `ChildCareExpress`.`System_Users` (`id`, `UserID`, `Password`, `PersonID`, `GroupID`) VALUES ('0', 'MANAGER', 'PASSWORD', '0', 'A');
INSERT INTO `ChildCareExpress`.`System_Users` (`id`, `UserID`, `Password`, `PersonID`, `GroupID`) VALUES ('1', 'JCAIN', 'PASSWORD', '2', 'U');
INSERT INTO `ChildCareExpress`.`ChildtoPerson` (`id`, `ChildID`, `PersonID`, `Ptype`) VALUES ('0', '0', '0', 'M');
INSERT INTO `ChildCareExpress`.`Person` (`id`, `Firstname`, `Lastname`, `SSN`, `Driverslic`, `MakesPayment`, `AddrID`, `EmployerID`)VALUES ('0', 'Default', 'Default', '000-00-0000', 'XXXXXXXX', 'N', '0', '0');
INSERT INTO `ChildCareExpress`.`Addresses` (`id`, `Address1`, `Address2`, `Email`, `Cities`, `Zipcodes`, `States`, `Hphone`, `Cphone`, `Wphone`)VALUES ('0', '00000 Default', null, 'Default@Default.com', 'Default', '00000', 'OH', '(000) 000 - 0000', '(000) 000 - 0000', '(000) 000 - 0000');

COMMIT;

