/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package childcareexpress;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Joshua Cain
 */
public class AddressEntry {
    private int intAddressId;
    private String strAddress1;
    private String strAddress2;
    private String strEmail;
    private String strCities;
    private String strZipcodes;
    private int strStates;
    private String strHphone;
    private String strCphone;
    databaseMgr dbcon;

    public AddressEntry() {}
    public AddressEntry(int AddrId) {
        intAddressId = AddrId;
        selectAddress();
    }

    /**
     * @return the intAddressId
     */
    public int getIntAddressId() {
        return intAddressId;
    }

    /**
     * @return the strAddress1
     */
    public String getStrAddress1() {
        return strAddress1;
    }

    /**
     * @return the strAddress2
     */
    public String getStrAddress2() {
        return strAddress2;
    }

    /**
     * @return the strEmail
     */
    public String getStrEmail() {
        return strEmail;
    }

    /**
     * @return the strCities
     */
    public String getStrCities() {
        return strCities;
    }

    /**
     * @return the strZipcodes
     */
    public String getStrZipcodes() {
        return strZipcodes;
    }

    /**
     * @return the strStates
     */
    public int getStrStates() {
        return strStates;
    }

    /**
     * @return the strHphone
     */
    public String getStrHphone() {
        return strHphone;
    }

    /**
     * @return the strCphone
     */
    public String getStrCphone() {
        return strCphone;
    }

    /**
     * @param intAddressId the intAddressId to set
     */
    public void setIntAddressId(int intAddressId) {
        this.intAddressId = intAddressId;
    }

    /**
     * @param strAddress1 the strAddress1 to set
     */
    public void setStrAddress1(String strAddress1) {
        this.strAddress1 = strAddress1;
    }

    /**
     * @param strAddress2 the strAddress2 to set
     */
    public void setStrAddress2(String strAddress2) {
        this.strAddress2 = strAddress2;
    }

    /**
     * @param strEmail the strEmail to set
     */
    public void setStrEmail(String strEmail) {
        this.strEmail = strEmail;
    }

    /**
     * @param strCities the strCities to set
     */
    public void setStrCities(String strCities) {
        this.strCities = strCities;
    }

    /**
     * @param strZipcodes the strZipcodes to set
     */
    public void setStrZipcodes(String strZipcodes) {
        this.strZipcodes = strZipcodes;
    }

    /**
     * @param strStates the strStates to set
     */
    public void setStrStates(int strStates) {
        this.strStates = strStates;
    }

    /**
     * @param strHphone the strHphone to set
     */
    public void setStrHphone(String strHphone) {
        this.strHphone = strHphone;
    }

    /**
     * @param strCphone the strCphone to set
     */
    public void setStrCphone(String strCphone) {
        this.strCphone = strCphone;
    }

    public void insertAddress(){
        try {
            intAddressId = dbcon.insertIntoAddresses(strAddress1, strAddress2, strEmail, strCities, strZipcodes, strStates, strHphone, strCphone);
            if (intAddressId == -1) {
                System.out.println("FAIL");
            }
        } catch (SQLException ex) {
            Logger.getLogger(AddressEntry.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void updateAddress(){
        try {
            dbcon.updateAddresses(intAddressId, strAddress1, strAddress2, strEmail, strCities, strZipcodes, strStates, strHphone, strCphone);
        } catch (SQLException ex) {
            Logger.getLogger(AddressEntry.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void selectAddress(){
        try {
            AddressEntry myAddress = dbcon.getAddresses(intAddressId);
            intAddressId = myAddress.getIntAddressId();
            strAddress1 = myAddress.getStrAddress1();
            strAddress2 = myAddress.getStrAddress2();
            strEmail = myAddress.getStrEmail();
            strCities = myAddress.strCities;
            strZipcodes = myAddress.strZipcodes;
            strStates = myAddress.strStates;
            strHphone = myAddress.strHphone;
            strCphone = myAddress.strCphone;
        } catch (SQLException ex) {
            Logger.getLogger(AddressEntry.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
