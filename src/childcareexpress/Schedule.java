/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package childcareexpress;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;

/**
 *
 * @author Joshua Cain
 */
public class Schedule {

    int Childid;
    int Count;
    String[] DayofMonth;
    long[] DateIn;
    long[] DateOut;
    long ts;

    public Schedule(int myCount) {
        Count = myCount;
        DateIn = new long[myCount];
        DateOut = new long[myCount];
        DayofMonth = new String[myCount];
    }

    Schedule() {
    }

    public long getTodayLong() {
        ts = (new Date()).getTime();
        return ts;
    }

    public DefaultListModel toList() {

        DefaultListModel listtimes = new DefaultListModel();
        Date inTime = new Date();
        String inAt = "";
        Date outTime = new Date();
        String outAt = "";
        for (int i = 0; i < Count; i++) {
            if (DateIn[i] == -1) {
                inAt = " NA";
            } else {
                inTime.setTime(DateIn[i]);
                inAt = inTime.toString();
            }
            if (DateOut[i] == -1) {
                outAt = " NA";
            } else {
                outTime.setTime(DateOut[i]);
                outAt = outTime.toString();
            }
            outTime.setTime(DateOut[i]);
            listtimes.addElement("Time-In: " + inAt + " Time-Out: " + outAt);
        }
        return listtimes;
    }
}
