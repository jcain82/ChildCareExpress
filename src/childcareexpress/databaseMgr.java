/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package childcareexpress;

import java.io.CharArrayReader;
import java.io.Reader;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class databaseMgr {

    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DATABASE_URL = "jdbc:mysql://localhost:3306/ChildCareExpress";
    static final String USERNAME = "root";
    static final String PASSWORD = "Password1";
    // reference to database connection
    private Connection connection;
    //Login Statements
    private PreparedStatement sqlLogin;
    private PreparedStatement sqlNewLogin;
    private PreparedStatement sqlUpdateLogin;
    //Employee Statements
    private PreparedStatement sqlEmployee;
    private PreparedStatement sqlNewEmployee;
    private PreparedStatement sqlUpdateEmployee;
    //Child Statements
    private PreparedStatement sqlChild;
    private PreparedStatement sqlChildSearchFname;
    private PreparedStatement sqlChildSearchLname;
    private PreparedStatement sqlChildSearchID;
    private PreparedStatement sqlChildGetAllergies;
    private PreparedStatement sqlChildGetMedical;
    private PreparedStatement sqlChildID;
    private PreparedStatement sqlChildDelete;
    private PreparedStatement sqlNewChild;
    private PreparedStatement sqlUpdateChild;
    private PreparedStatement sqlNewChildAllergies;
    private PreparedStatement sqlNewChildMed;
    private PreparedStatement sqlChildCheckedIn;
    private PreparedStatement sqlChildCheckedOut;
    private PreparedStatement sqlUpdateAllergies;
    private PreparedStatement sqlUpdateMedical;
    //Parent Statements
    private PreparedStatement sqlGuardian;
    private PreparedStatement sqlNewGuardian;
    private PreparedStatement sqlUpdateGuardian;
    //Address Statements
    private PreparedStatement sqlAddress;
    private PreparedStatement sqlNewAddress;
    private PreparedStatement sqlUpdateAddress;

    // set up PreparedStatements to access database
    public databaseMgr() throws Exception {
        // connect to database
        connect();

        //Version 2.0 Functionality
        // Find User
        //sqlLogin = connection.prepareStatement("SELECT id, UserID, Password " + "FROM system_users " + "WHERE UserID = ? ");
        //sqlNewLogin = connection.prepareStatement("INSERT INTO system_users (id, UserID, Password, PersonID, GroupID)" + "VALUES ( ?, ?, ?, ?,");
        //sqlUpdateLogin = connection.prepareStatement("UPDATE system_users SET UserID = ?, Password = ?, PersonID = ?, GroupID = ? WHERE id = ?");

        //sqlEmployee = connection.prepareStatement("SELECT id, UserID, Password " + "FROM system_users " + "WHERE UserID = ? ");
        //sqlNewEmployee = connection.prepareStatement("INSERT INTO system_users (UserID, Password, PersonID, GroupID)" + "VALUES ( ?, ?, ?, ?)");
        //sqlUpdateEmployee = connection.prepareStatement("UPDATE system_users SET UserID = ?, Password = ?, PersonID = ?, GroupID = ? WHERE id = ?");

        sqlAddress = connection.prepareStatement("SELECT Address1, Address2, Cities, States, Zipcodes, Email " + "FROM Addresses " + "WHERE id = ? ");
        sqlNewAddress = connection.prepareStatement("INSERT INTO Addresses (id, Address1, Address2, Cities, States, Zipcodes, Email)" + "VALUES ( ?, ?, ?, ?, ?, ?, ?");
        sqlUpdateAddress = connection.prepareStatement("INSERT INTO Addresses (Address1 = ?, Address2 = ?, Cities = ?, States = ?, Zipcodes = ?, Email = ? WHERE id = ? ");

        sqlChild = connection.prepareStatement("SELECT Firstname, Middlename, Lastname, DOB, Sex, Enrolled, Transport, MedConid " + "FROM Child " + "WHERE id = ? ");
        sqlChildSearchFname = connection.prepareStatement("SELECT id, Firstname, Middlename, Lastname, DOB, Sex, Enrolled, Transport, MedConid " + "FROM Child " + "WHERE Firstname like ?");
        sqlChildSearchLname = connection.prepareStatement("SELECT id, Firstname, Middlename, Lastname, DOB, Sex, Enrolled, Transport, MedConid " + "FROM Child " + "WHERE Lastname like ?");
        sqlChildSearchID = connection.prepareStatement("SELECT id, Firstname, Middlename, Lastname, DOB, Sex, Enrolled, Transport, MedConid " + "FROM Child " + "WHERE id = ?");
        sqlChildID = connection.prepareStatement("SELECT id " + "FROM Child " + "WHERE Firstname = ? AND Lastname = ? ");
        sqlChildDelete = connection.prepareStatement("DELETE FROM Child WHERE id = ?");
        sqlNewChild = connection.prepareStatement("INSERT INTO Child (Firstname, Middlename, Lastname, DOB, Sex, Enrolled, Transport, MedConid) " + "VALUES ( ?, ?, ?, ?, ?, ?, ?, ? )");
        sqlNewChildAllergies = connection.prepareStatement("INSERT INTO Allergies (Childid, Notes) " + "VALUES ( ?, ? )");
        sqlNewChildMed = connection.prepareStatement("INSERT INTO MedicalCondition (Childid, Notes) " + "VALUES ( ?, ? )");
        sqlUpdateChild = connection.prepareStatement("UPDATE Child SET Firstname = ?, Middlename = ?, Lastname = ?, DOB = ? Sex = ?, Enrolled = ?, Transport = ?, MedConid = ?" + " WHERE id = ? ");
        sqlChildGetAllergies = connection.prepareStatement("SELECT Notes " + "FROM Allergies " + "WHERE Childid = ? ");
        sqlChildGetMedical = connection.prepareStatement("SELECT Notes " + "FROM MedicalCondition " + "WHERE Childid = ? ");
        sqlUpdateAllergies = connection.prepareStatement("UPDATE Allergies SET Notes = ?" + " WHERE Childid = ? ");
        sqlUpdateMedical = connection.prepareStatement("UPDATE MedicalCondition SET Notes=? " + " WHERE Childid = ? ");

    }  // end DataAccess constructor

    // Obtain a connection to database. Method may
    // may throw ClassNotFoundException or SQLException. If so,
    // exception is passed via this class's constructor back to
    // the AddressBook application so the application can display
    // an error message and terminate.
    private void connect() throws Exception {

        // load database driver class
        Class.forName(JDBC_DRIVER);

        // establish connection to database
        connection = DriverManager.getConnection(DATABASE_URL, USERNAME, PASSWORD);

        // Require manual commit for transactions. This enables
        // the program to rollback transactions that do not
        // complete and commit transactions that complete properly.
        connection.setAutoCommit(false);
    }

    // Locate specified person. Method returns AddressBookEntry
    // containing information.
    public Employee checkLogin(Employee User) {
        try {
            sqlLogin.setString(1, User.getUserID());
            ResultSet temp = sqlLogin.executeQuery();
            temp.next();
            if (temp.first()) {
                if (temp.getString(2) != null) {
                    if ((temp.getString(2).equals(User.getUserID())) && (temp.getString(3).equals(User.getPassword()))) {
                        User.setValidUser(true);
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return User;

    }  // end method login

    // method to close statements and database connection
    public void close() {
        // close database connection
        try {
            sqlLogin.close();
            sqlAddress.close();
            connection.close();
        } // end try
        // detect problems closing statements and connection
        catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }  // end method close

    // Method to clean up database connection. Provided in case
    // DataAccess object is garbage collected.
    protected void finalize() {
        close();
    }

    public int AddChild(Child myChild) {
        try {
            sqlNewChild.setString(1, myChild.Firstname);
            sqlNewChild.setString(2, myChild.Middlename);
            sqlNewChild.setString(3, myChild.Lastname);
            sqlNewChild.setString(4, myChild.DOB);
            if (myChild.Sex == 'M') {
                sqlNewChild.setString(5, "M");
            } else {
                sqlNewChild.setString(5, "F");
            }
            sqlNewChild.setString(6, myChild.Enrolled);
            if (myChild.Transport == 'Y') {
                sqlNewChild.setString(7, "Y");
            } else {
                sqlNewChild.setString(7, "N");
            }
            sqlNewChild.setInt(8, myChild.MedConId);
            sqlNewChild.executeUpdate();
            connection.commit();
            sqlChildID.setString(1, myChild.Firstname);
            sqlChildID.setString(2, myChild.Lastname);
            ResultSet temp = sqlChildID.executeQuery();
            temp.next();
            if (temp.first()) {
                myChild.ChildID = temp.getInt(1);
            } else {
                throw new Exception("INVALID");
            }

            if (myChild.Allergy == 'Y') {
                sqlNewChildAllergies.setInt(1, myChild.ChildID);
                sqlNewChildAllergies.setString(2, myChild.AllergyNotes);
                sqlNewChildAllergies.executeUpdate();
                connection.commit();
            }
            if (myChild.MedCondition == 'Y') {
                sqlNewChildMed.setInt(1, myChild.ChildID);
                sqlNewChildMed.setString(2, myChild.MedConditionNotes);
                sqlNewChildMed.executeUpdate();
                connection.commit();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return myChild.ChildID;
    }

    public void insertIntoAllergies(int Childid, String allergyNotes) throws SQLException {
        try {
            connect();
            String sql = "INSERT INTO medicalcondition (Childid, Notes)" + "VALUES (?, ?)";
            PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, allergyNotes);
            statement.execute();
            connection.commit();
            statement.close();
            connection.close();
        } catch (Exception ex) {
            Logger.getLogger(databaseMgr.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void updateAllergies(int keyId, String allergyNotes) throws SQLException {
        try {
            connect();
            String sql = "SELECT * FROM allergies WHERE childID = ?";
            PreparedStatement statement = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            statement.setInt(1, keyId);
            ResultSet entry = statement.executeQuery();
            entry.last();
            int rows = entry.getRow();
            entry.beforeFirst();
            if (rows == 0) {
                entry.close();
                statement.close();
                connection.close();
                insertIntoAllergies(keyId, allergyNotes);
            } else {
                entry.next();
                if (allergyNotes != null) {
                    entry.updateString("Notes", allergyNotes);
                }
                entry.updateRow();
                entry.close();
                connection.commit();
                statement.close();
                connection.close();
            }
        } catch (Exception ex) {
            Logger.getLogger(databaseMgr.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void updateChild(int keyId, String firstname, String middlename, String lastname, String dOB, char sex,
            String enrolled, char transport) throws SQLException {
        try {
            connect();
            String sql = "SELECT * FROM Child WHERE id = ?";
            PreparedStatement statement = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            statement.setInt(1, keyId);
            ResultSet entry = statement.executeQuery();
            entry.last();
            int rows = entry.getRow();
            entry.beforeFirst();
            if (rows == 0) {
                entry.close();
                statement.close();
                connection.close();
            }
            entry.next();
            if (firstname != null) {
                entry.updateString("firstname", firstname);
            }
            if (middlename != null) {
                entry.updateString("middlename", middlename);
            }
            if (lastname != null) {
                entry.updateString("lastname", lastname);
            }
            if (dOB != null) {
                entry.updateString("DOB", dOB);
            }
            entry.updateString("Sex", String.valueOf(sex));
            if (enrolled != null) {
                entry.updateString("enrolled", enrolled);
            }
            entry.updateString("transport", String.valueOf(transport));
            entry.updateRow();
            entry.close();
            connection.commit();
            statement.close();
            connection.close();
        } catch (Exception ex) {
            Logger.getLogger(databaseMgr.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void insertIntoMedicalcondition(int ChildID, String medConditionNotes) throws SQLException {
        try {
            connect();
            String sql = "INSERT INTO medicalcondition (childid, Notes)" + "VALUES (? , ?)";
            PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1, ChildID);
            statement.setString(2, medConditionNotes);
            statement.execute();
            connection.commit();
            statement.close();
            connection.close();
        } catch (Exception ex) {
            Logger.getLogger(databaseMgr.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void updateMedicalcondition(int keyId, String medConditionNotes) throws SQLException {
        try {
            connect();
            String sql = "SELECT * FROM medicalcondition WHERE childID = ?";
            PreparedStatement statement = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            statement.setInt(1, keyId);
            ResultSet entry = statement.executeQuery();
            entry.last();
            int rows = entry.getRow();
            entry.beforeFirst();
            if (rows == 0) {
                entry.close();
                statement.close();
                connection.close();
                insertIntoMedicalcondition(keyId, medConditionNotes);
            } else {
                entry.next();
                if (medConditionNotes != null) {
                    entry.updateString("Notes", medConditionNotes);
                }
                entry.updateRow();
                entry.close();
                connection.commit();
                statement.close();
                connection.close();
            }
        } catch (Exception ex) {
            Logger.getLogger(databaseMgr.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public Child[] Search(String text) {
        Child[] myChild;
        ResultSet temp;
        ResultSet temp2;
        try {
            switch (text.charAt(0)) {
                case 'F': {
                    sqlChildSearchFname.setString(1, text.subSequence(2, text.length() - 1) + "%");
                    temp = sqlChildSearchFname.executeQuery();
                    break;
                }
                case 'L': {
                    sqlChildSearchLname.setString(1, "%" + text.subSequence(2, text.length() - 1) + "%");
                    temp = sqlChildSearchLname.executeQuery();
                    break;
                }
                case 'I': {
                    sqlChildSearchID.setInt(1, Integer.parseInt("" + text.subSequence(2, text.length() - 1)));
                    temp = sqlChildSearchID.executeQuery();
                    break;
                }
                case 'E': {
                    sqlChildSearchFname.setString(1, "%");
                    temp = sqlChildSearchFname.executeQuery();
                    break;
                }
                default: {
                    sqlChildSearchFname.setString(1, text + "%");
                    temp = sqlChildSearchFname.executeQuery();
                    break;
                }
            }
            temp.last();
            int count = temp.getRow();
            if (count > 0) {
                myChild = new Child[count];
                temp.first();
                for (int i = 0; i < count; i++) {
                    myChild[i] = new Child();
                    myChild[i].ChildID = temp.getInt("id");
                    myChild[i].Firstname = temp.getString("Firstname");
                    myChild[i].Middlename = temp.getString("Middlename");
                    myChild[i].Lastname = temp.getString("Lastname");
                    myChild[i].DOB = temp.getString("DOB");
                    myChild[i].Sex = temp.getString("Sex").charAt(0);
                    myChild[i].Enrolled = temp.getString("Enrolled");
                    myChild[i].Transport = temp.getString("Transport").charAt(0);
                    myChild[i].MedConId = temp.getInt("MedConid");
                    myChild[i].myRelation = this.selectChildtoperson(myChild[i].ChildID);
                    sqlChildGetAllergies.setInt(1, myChild[i].ChildID);
                    temp2 = sqlChildGetAllergies.executeQuery();
                    if (temp2.next()) {
                        temp2.first();
                        myChild[i].AllergyNotes = temp2.getString("Notes");
                    }
                    sqlChildGetMedical.setInt(1, myChild[i].ChildID);
                    temp2 = sqlChildGetMedical.executeQuery();

                    if (temp2.next()) {
                        temp2.first();
                        myChild[i].MedConditionNotes = temp2.getString("Notes");
                    }
                    temp.next();
                }

                return myChild;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return (new Child[0]);
    }

    public Child[] CheckOut(String text) {
        Child[] myChild;
        ResultSet temp;
        ResultSet temp2;
        try {
            switch (text.charAt(0)) {
                case 'E': {
                    sqlChildSearchFname.setString(1, "%");
                    temp = sqlChildSearchFname.executeQuery();
                    break;
                }
                default: {
                    sqlChildCheckedIn.setString(1, text.subSequence(2, text.length() - 1) + "%");
                    temp = sqlChildSearchFname.executeQuery();
                    break;
                }
            }
            temp.last();
            int count = temp.getRow();
            if (count > 0) {
                myChild = new Child[count];
                temp.first();
                for (int i = 0; i < count; i++) {
                    myChild[i] = new Child();
                    myChild[i].ChildID = temp.getInt("id");
                    myChild[i].Firstname = temp.getString("Firstname");
                    myChild[i].Middlename = temp.getString("Middlename");
                    myChild[i].Lastname = temp.getString("Lastname");
                    myChild[i].DOB = temp.getString("DOB");
                    myChild[i].Sex = temp.getString("Sex").charAt(0);
                    myChild[i].Enrolled = temp.getString("Enrolled");
                    myChild[i].Transport = temp.getString("Transport").charAt(0);
                    myChild[i].MedConId = temp.getInt("MedConid");
                    sqlChildGetAllergies.setInt(1, myChild[i].ChildID);
                    temp2 = sqlChildGetAllergies.executeQuery();
                    if (temp2.next()) {
                        temp2.first();
                        myChild[i].AllergyNotes = temp2.getString("Notes");
                    }
                    sqlChildGetMedical.setInt(1, myChild[i].ChildID);
                    temp2 = sqlChildGetMedical.executeQuery();

                    if (temp2.next()) {
                        temp2.first();
                        myChild[i].MedConditionNotes = temp2.getString("Notes");
                    }
                }

                return myChild;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return (new Child[0]);
    }
    /* public void Checkout(int keyId) throws SQLException {
    try {
    connect();

    statement.setInt(1, keyId);
    statement.executeUpdate();
    statement.close();
    connection.close();
    } catch (Exception ex) {
    Logger.getLogger(databaseMgr.class.getName()).log(Level.SEVERE, null, ex);
    }
    }*/

    public int insertIntoAddresses(String strAddress1, String strAddress2, String strEmail, String strCities, String strZipcodes,
            int strStates, String strHphone, String strCphone) throws SQLException {
        int generatedId = -1;
        try {
            connect();
            String sql = "INSERT INTO addresses (Address1, Address2, Email, Cities, Zipcodes, States, " + "Hphone, Cphone)" + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, strAddress1);
            statement.setString(2, strAddress2);
            statement.setString(3, strEmail);
            statement.setString(4, strCities);
            statement.setString(5, strZipcodes);
            statement.setInt(6, strStates);
            statement.setString(7, strHphone);
            statement.setString(8, strCphone);
            statement.execute();
            ResultSet auto = statement.getGeneratedKeys();
            if (auto.next()) {
                generatedId = auto.getInt(1);
            } else {
                generatedId = -1;
            }
            statement.close();
            connection.commit();
            connection.close();
        } catch (Exception ex) {
            Logger.getLogger(databaseMgr.class.getName()).log(Level.SEVERE, null, ex);
        }
        return generatedId;
    }

    public void updateAddresses(int keyId, String strAddress1, String strAddress2, String strEmail, String strCities, String strZipcodes,
            int strStates, String strHphone, String strCphone) throws SQLException {
        try {
            String sql = "SELECT * FROM Addresses WHERE id = ?";
            connect();
            PreparedStatement statement = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            statement.setInt(1, keyId);
            ResultSet entry = statement.executeQuery();
            entry.last();
            int rows = entry.getRow();
            entry.beforeFirst();
            if (rows == 0) {
                entry.close();
                statement.close();
                connection.close();
            }
            entry.next();
            if (strAddress1 != null) {
                entry.updateString("Address1", strAddress1);
            }
            if (strAddress2 != null) {
                entry.updateString("Address2", strAddress2);
            }
            if (strEmail != null) {
                entry.updateString("Email", strEmail);
            }
            if (strCities != null) {
                entry.updateString("Cities", strCities);
            }
            if (strZipcodes != null) {
                entry.updateString("Zipcodes", strZipcodes);
            }
            if (strStates != -1) {
                entry.updateInt("States", strStates);
            }
            if (strHphone != null) {
                entry.updateString("Hphone", strHphone);
            }
            if (strCphone != null) {
                entry.updateString("Cphone", strCphone);
            }
            entry.updateRow();
            entry.close();
            connection.commit();
            statement.close();
            connection.close();
        } catch (Exception ex) {
            Logger.getLogger(databaseMgr.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public AddressEntry getAddresses(int keyId) throws SQLException {
        AddressEntry myAddress = new AddressEntry();
        try {
            String sql = "SELECT * FROM Addresses WHERE id = ?";
            connect();
            PreparedStatement statement = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            statement.setInt(1, keyId);
            ResultSet entry = statement.executeQuery();
            entry.last();
            int rows = entry.getRow();
            entry.beforeFirst();
            if (rows == 0) {
                entry.close();
                statement.close();
                connection.close();
            }
            entry.next();
            myAddress.setIntAddressId(keyId);
            myAddress.setStrAddress1(entry.getString("Address1"));
            myAddress.setStrAddress2(entry.getString("Address2"));
            myAddress.setStrEmail(entry.getString("Email"));
            myAddress.setStrCities(entry.getString("Cities"));
            myAddress.setStrZipcodes(entry.getString("Zipcodes"));
            myAddress.setStrStates(entry.getInt("States"));
            myAddress.setStrHphone(entry.getString("Hphone"));
            myAddress.setStrCphone(entry.getString("Cphone"));
            entry.close();
            statement.close();
            connection.close();
        } catch (Exception ex) {
            Logger.getLogger(databaseMgr.class.getName()).log(Level.SEVERE, null, ex);
        }
        return myAddress;
    }

    public void deleteFromAddresses(int keyId) throws SQLException {
        try {
            connect();
            String sql = "DELETE FROM Addresses WHERE id = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, keyId);
            statement.executeUpdate();
            statement.close();
            connection.commit();
            connection.close();
        } catch (Exception ex) {
            Logger.getLogger(databaseMgr.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int insertIntoEmployer(String employerName, int eAddrID, String workphone) throws SQLException {
        int generatedId = -1;
        try {

            connect();
            String sql = "INSERT INTO employer (Name, AddrID, Wphone)" + "VALUES (?, ?, ?)";
            PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, employerName);
            statement.setInt(2, eAddrID);
            statement.setString(3, workphone);
            statement.execute();
            ResultSet auto = statement.getGeneratedKeys();
            if (auto.next()) {
                generatedId = auto.getInt(1);
            } else {
                generatedId = -1;
            }
            connection.commit();
            statement.close();
            connection.close();

        } catch (Exception ex) {
            Logger.getLogger(databaseMgr.class.getName()).log(Level.SEVERE, null, ex);
        }
        return generatedId;
    }

    public void updateEmployer(int keyId, String employerName, int eAddrID, String workphone) throws SQLException {
        try {
            connect();
            String sql = "SELECT * FROM employer WHERE id = ?";
            PreparedStatement statement = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            statement.setInt(1, keyId);
            ResultSet entry = statement.executeQuery();
            entry.last();
            int rows = entry.getRow();
            entry.beforeFirst();
            if (rows == 0) {
                entry.close();
                statement.close();
                connection.close();
            }
            entry.next();
            if (employerName != null) {
                entry.updateString("Name", employerName);
            }
            entry.updateInt("AddrID", eAddrID);
            if (workphone != null) {
                entry.updateString("Wphone", workphone);
            }
            entry.updateRow();
            entry.close();
            connection.commit();
            statement.close();
            connection.close();
        } catch (Exception ex) {
            Logger.getLogger(databaseMgr.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Employer getEmployer(int keyId) throws SQLException {
        Employer myEmployer = new Employer();
        try {
            String sql = "SELECT * FROM employer WHERE id = ?";
            connect();
            PreparedStatement statement = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            statement.setInt(1, keyId);
            ResultSet entry = statement.executeQuery();
            entry.last();
            int rows = entry.getRow();
            entry.beforeFirst();
            if (rows == 0) {
                entry.close();
                statement.close();
                connection.close();
            } else {
                entry.next();
                myEmployer.setEmployerID(keyId);
                myEmployer.setEmployerName(entry.getString("Name"));
                myEmployer.setEAddrID(entry.getInt("AddrID"));
                myEmployer.setWorkphone(entry.getString("Wphone"));
                entry.close();
                statement.close();
                connection.close();
            }
        } catch (Exception ex) {
            Logger.getLogger(databaseMgr.class.getName()).log(Level.SEVERE, null, ex);
        }
        return myEmployer;
    }

    public void deleteFromEmployer(int keyId) throws SQLException {
        try {
            String sql = "DELETE FROM employer WHERE id = ?";
            connect();
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, keyId);
            statement.executeUpdate();
            connection.commit();
            statement.close();
            connection.close();
        } catch (Exception ex) {
            Logger.getLogger(databaseMgr.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int insertIntoPerson(String firstname, String lastname, String sSN, String dOB, String driverslic,
            int addrID, int employID) throws SQLException {
        int generatedId = -1;
        try {

            connect();
            String sql = "INSERT INTO person (Firstname, Lastname, SSN, DOB, Driverslic, AddrID, " + "EmployerID)" + "VALUES (?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, firstname);
            statement.setString(2, lastname);
            statement.setString(3, sSN);
            statement.setString(4, dOB);
            statement.setString(5, driverslic);
            statement.setInt(6, addrID);
            statement.setInt(7, employID);
            statement.execute();
            ResultSet auto = statement.getGeneratedKeys();
            if (auto.next()) {
                generatedId = auto.getInt(1);
            } else {
                generatedId = -1;
            }

            connection.commit();
            statement.close();
            connection.close();

        } catch (Exception ex) {
            Logger.getLogger(databaseMgr.class.getName()).log(Level.SEVERE, null, ex);
        }
        return generatedId;
    }

    public void updatePerson(int keyId, String firstname, String lastname, String sSN, String dOB, String driverslic,
            int addrID, int employID) throws SQLException {
        try {
            connect();
            String sql = "SELECT * FROM person WHERE id = ?";
            PreparedStatement statement = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            statement.setInt(1, keyId);
            ResultSet entry = statement.executeQuery();
            entry.last();
            int rows = entry.getRow();
            entry.beforeFirst();
            if (rows == 0) {
                entry.close();
                statement.close();
                connection.close();
            }
            entry.next();
            if (firstname != null) {
                entry.updateString("Firstname", firstname);
            }
            if (lastname != null) {
                entry.updateString("Lastname", lastname);
            }
            if (sSN != null) {
                entry.updateString("SSN", sSN);
            }
            if (dOB != null) {
                entry.updateString("DOB", dOB);
            }
            if (driverslic != null) {
                entry.updateString("Driverslic", driverslic);
            }
            entry.updateInt("AddrID", addrID);
            entry.updateInt("EmployerID", employID);
            entry.updateRow();
            entry.close();
            connection.commit();
            statement.close();
            connection.close();
        } catch (Exception ex) {
            Logger.getLogger(databaseMgr.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Person getPerson(int keyId) throws SQLException {
        Person myPerson = new Person();
        try {
            connect();
            String sql = "SELECT * FROM person WHERE id = ?";
            PreparedStatement statement = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            statement.setInt(1, keyId);
            ResultSet entry = statement.executeQuery();
            entry.last();
            int rows = entry.getRow();
            entry.beforeFirst();
            if (rows == 0) {
                entry.close();
                statement.close();
                connection.close();
            }
            entry.next();
            myPerson.setPersonId(keyId);
            myPerson.setFirstname(entry.getString("Firstname"));
            myPerson.setLastname(entry.getString("Lastname"));
            myPerson.setSSN(entry.getString("SSN"));
            myPerson.setDOB(entry.getString("DOB"));
            myPerson.setDriverslic(entry.getString("Driverslic"));
            myPerson.setAddrID(entry.getInt("AddrID"));
            myPerson.setEmployID(entry.getInt("EmployerID"));

            entry.close();
            statement.close();
            connection.close();
        } catch (Exception ex) {
            Logger.getLogger(databaseMgr.class.getName()).log(Level.SEVERE, null, ex);
        }
        return myPerson;
    }

    public void deleteFromPerson(int keyId) throws SQLException {
        try {
            connect();
            String sql = "DELETE FROM person WHERE id = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, keyId);
            statement.executeUpdate();
            statement.close();
            connection.commit();
            connection.close();
        } catch (Exception ex) {
            Logger.getLogger(databaseMgr.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int insertIntoMedicalContacts(String name, String phone, int addrId) throws SQLException {
        int generatedId = -1;
        try {

            connect();
            String sql = "INSERT INTO MedicalContacts (Name, Phone, AddrId)" + "VALUES (?, ?, ?)";
            PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, name);
            statement.setString(2, phone);
            statement.setInt(3, addrId);
            statement.execute();
            ResultSet auto = statement.getGeneratedKeys();
            if (auto.next()) {
                generatedId = auto.getInt(1);
            } else {
                generatedId = -1;
            }
            connection.commit();
            statement.close();
            connection.close();

        } catch (Exception ex) {
            Logger.getLogger(databaseMgr.class.getName()).log(Level.SEVERE, null, ex);
        }
        return generatedId;
    }

    public MedicalContact selectMedicalContacts(int keyId) throws SQLException {
        MedicalContact medCon = new MedicalContact();
        try {
            connect();
            String sql = "SELECT * FROM MedicalContacts WHERE id = ?";
            PreparedStatement statement = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            statement.setInt(1, keyId);
            ResultSet entry = statement.executeQuery();
            entry.last();
            int rows = entry.getRow();
            entry.beforeFirst();
            if (rows == 0) {
                entry.close();
                statement.close();
                connection.close();
            }
            entry.next();
            medCon.setIntMedConId(keyId);
            medCon.setName(entry.getString("Name"));
            medCon.setPhone(entry.getString("Phone"));
            medCon.setAddrId(entry.getInt("AddrId"));
            entry.close();
            connection.commit();
            statement.close();
            connection.close();
        } catch (Exception ex) {
            Logger.getLogger(databaseMgr.class.getName()).log(Level.SEVERE, null, ex);
        }
        return medCon;
    }

    public void updateMedicalContacts(int keyId, String name, String phone, int addrId) throws SQLException {
        try {
            connect();
            String sql = "SELECT * FROM MedicalContacts WHERE id = ?";
            PreparedStatement statement = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            statement.setInt(1, keyId);
            ResultSet entry = statement.executeQuery();
            entry.last();
            int rows = entry.getRow();
            entry.beforeFirst();
            if (rows == 0) {
                entry.close();
                statement.close();
                connection.close();
            }
            entry.next();
            if (name != null) {
                entry.updateString("name", name);
            }
            if (phone != null) {
                entry.updateString("phone", phone);
            }
            entry.updateInt("addrId", addrId);
            entry.updateRow();
            entry.close();
            connection.commit();
            statement.close();
            connection.close();
        } catch (Exception ex) {
            Logger.getLogger(databaseMgr.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void deleteFromMedicalContacts(int keyId) throws SQLException {
        try {
            connect();
            String sql = "DELETE FROM MedicalContacts WHERE id = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, keyId);
            statement.executeUpdate();
            connection.commit();
            statement.close();
            connection.close();
        } catch (Exception ex) {
            Logger.getLogger(databaseMgr.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void insertIntoChildtoperson(int childid, int personid) throws SQLException {
        try {
            connect();
            String sql = "INSERT INTO childtoperson (ChildID, PersonID)" + "VALUES (?, ?)";
            PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            statement.setInt(1, childid);
            statement.setInt(2, personid);
            statement.execute();

            connection.commit();
            statement.close();
            connection.close();

        } catch (Exception ex) {
            Logger.getLogger(databaseMgr.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Relationship selectChildtoperson(int childid) throws SQLException {
        Relationship myRelation = new Relationship();
        try {
            connect();
            String sql = "SELECT * FROM childtoperson WHERE ChildID = ?";
            PreparedStatement statement = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            statement.setInt(1, childid);
            ResultSet entry = statement.executeQuery();
            entry.last();
            int rows = entry.getRow();
            entry.beforeFirst();
            if (rows == 0) {
                entry.close();
                statement.close();
                connection.close();
            }
            int i = 0;
            while (!entry.isLast()) {
                entry.next();
                myRelation.Childid = entry.getInt("ChildID");
                myRelation.Personid[i] = entry.getInt("PersonID");
                i++;
            }
            entry.close();
            statement.close();
            connection.close();
        } catch (Exception ex) {
            Logger.getLogger(databaseMgr.class.getName()).log(Level.SEVERE, null, ex);
        }
        return myRelation;
    }

    public void deleteFromChildtoperson(int keyId) throws SQLException {
        try {
            connect();
            String sql = "DELETE FROM childtoperson WHERE ChildID = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, keyId);
            statement.executeUpdate();
            connection.commit();
            statement.close();
            connection.close();
        } catch (Exception ex) {
            Logger.getLogger(databaseMgr.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void insertIntoHourslogged(int id, String today, long dateIn, long dateOut) throws SQLException {
        try {
            connect();
            String sql = "INSERT INTO hourslogged (Childid, DateIn, Timein, Timeout)" + "VALUES (?, ?, ?, ?)";
            PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setLong(1, id);
            statement.setString(2, today);
            statement.setLong(3, dateIn);
            statement.setLong(4, dateOut);
            statement.execute();
            ResultSet auto = statement.getGeneratedKeys();
            connection.commit();
            statement.close();
            connection.close();
        } catch (Exception ex) {
            Logger.getLogger(databaseMgr.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void updateHourslogged(int keyId, String today, long dateIn, long dateOut) throws SQLException {
        try {
            connect();
            String sql = "SELECT * FROM hourslogged WHERE childid = ? and DateIn = ?";
            PreparedStatement statement = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            statement.setInt(1, keyId);
            statement.setString(2, today);
            ResultSet entry = statement.executeQuery();
            entry.last();
            int rows = entry.getRow();
            entry.beforeFirst();
            if (rows == 0) {
                entry.close();
                statement.close();
                connection.close();
            }
            entry.next();
            entry.updateLong("Timein", dateIn);
            entry.updateLong("TimeOut", dateOut);
            entry.updateRow();
            entry.close();
            connection.commit();
            statement.close();
            connection.close();
        } catch (Exception ex) {
            Logger.getLogger(databaseMgr.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Schedule selectHourslogged(int keyId) throws SQLException {
        Schedule mySchedule = new Schedule();
        try {
            connect();
            String sql = "SELECT * FROM hourslogged WHERE childid = ?";
            PreparedStatement statement = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            statement.setInt(1, keyId);
            ResultSet entry = statement.executeQuery();
            entry.last();
            int rows = entry.getRow();
            mySchedule = new Schedule(rows);
            entry.beforeFirst();
            if (rows == 0) {
                entry.close();
                statement.close();
                connection.close();
                return null;
            }
            entry.first();
            int i = 0;
            while (!entry.isLast()) {
                mySchedule.DayofMonth[i] = entry.getString("DateIn");
                mySchedule.DateIn[i] = entry.getLong("Timein");
                mySchedule.DateOut[i] = entry.getLong("Timeout");
                i++;
            }
            entry.next();

            entry.close();
            connection.commit();
            statement.close();
            connection.close();
        } catch (Exception ex) {
            Logger.getLogger(databaseMgr.class.getName()).log(Level.SEVERE, null, ex);
        }
        return mySchedule;
    }

    public Child selectHourslogged(Child myChild) throws SQLException {

        try {
            connect();
            String sql = "SELECT * FROM hourslogged WHERE childid = ? and datein = ? ";
            PreparedStatement statement = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            statement.setInt(1, myChild.ChildID);
            statement.setString(2, myChild.now());
            ResultSet entry = statement.executeQuery();
            entry.last();
            int rows = entry.getRow();
            entry.beforeFirst();
            if (rows == 0) {
                entry.close();
                statement.close();
                connection.close();
                myChild.toDay = myChild.now();
                myChild.Checkin = -1;
                myChild.Checkout = -1;
                return myChild;
            }
            entry.first();
            myChild.toDay = myChild.now();
            myChild.Checkin = entry.getLong("Timein");
            myChild.Checkout = entry.getLong("Timeout");
            entry.close();
            connection.commit();
            statement.close();
            connection.close();
        } catch (Exception ex) {
            Logger.getLogger(databaseMgr.class.getName()).log(Level.SEVERE, null, ex);
        }
        return myChild;
    }

    public void deleteFromHourslogged(int keyId) throws SQLException {
        try {
            connect();
            String sql = "DELETE FROM hourslogged WHERE childid = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, keyId);
            statement.executeUpdate();
            connection.commit();
            statement.close();
            connection.close();
        } catch (Exception ex) {
            Logger.getLogger(databaseMgr.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}  // end class DataAccess

