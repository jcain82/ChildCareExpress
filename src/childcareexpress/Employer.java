package childcareexpress;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Joshua Cain
 */
class Employer {
    private int EmployerID;
    private String EmployerName;
    private int EAddrID;
    private AddressEntry address;
    private String Workphone;
    databaseMgr dbcon;
    /**
     * @return the EmployerID
     */
    public int getEmployerID() {
        return EmployerID;
    }

    /**
     * @param EmployerID the EmployerID to set
     */
    public void setEmployerID(int EmployerID) {
        this.EmployerID = EmployerID;
    }

    /**
     * @return the EmployerName
     */
    public String getEmployerName() {
        return EmployerName;
    }

    /**
     * @param EmployerName the EmployerName to set
     */
    public void setEmployerName(String EmployerName) {
        this.EmployerName = EmployerName;
    }

    /**
     * @return the EAddrID
     */
    public int getEAddrID() {
        return EAddrID;
    }

    /**
     * @param EAddrID the EAddrID to set
     */
    public void setEAddrID(int EAddrID) {
        this.EAddrID = EAddrID;
    }


    /**
     * @return the Work phone
     */
    public String getWorkphone() {
        return Workphone;
    }

    /**
     * @param Workphone the Work phone to set
     */
    public void setWorkphone(String Workphone) {
        this.Workphone = Workphone;
    }

    /**
     * @return the address
     */
    public AddressEntry getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(AddressEntry address) {
        this.address = address;
    }

    public void insertEmployer(){
        try {
            address.insertAddress();
            EAddrID = address.getIntAddressId();
            setEmployerID(dbcon.insertIntoEmployer(EmployerName, EAddrID, Workphone));
        } catch (SQLException ex) {
            Logger.getLogger(AddressEntry.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void updateEmployer(){
        try {
            dbcon.updateEmployer(EmployerID, EmployerName, EAddrID, Workphone);
        } catch (SQLException ex) {
            Logger.getLogger(AddressEntry.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void selectEmployer(){
        try {
            Employer myEmploy = dbcon.getEmployer(EmployerID);
        } catch (SQLException ex) {
            Logger.getLogger(AddressEntry.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
