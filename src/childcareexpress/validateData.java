/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package childcareexpress;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;

/**
 *
 * @author Joshua Cain
 */
public class validateData {

    public validateData() {
    }

    validateData(String text) {
        checkSearchString(text);
    }

    public boolean checkAlphaNumeric(String strString) {
        Matcher myCheck = ALPHANUMERIC.matcher(strString);
        return myCheck.matches();
    }

    public boolean checkAlpha(String strString) {
        Matcher myCheck = ALPHA.matcher(strString);
        return myCheck.matches();
    }

    public boolean checkNumeric(String strString) {
        Matcher myCheck = NUMERIC.matcher(strString);
        return myCheck.matches();
    }

    public boolean checkSearchString(String strString) {
        if (strString.charAt(2) == '{') {
            char upper = Character.toUpperCase(strString.charAt(1));
            switch (upper) {
                case 'C':
                    return checkAlphaNumeric(strString.substring(strString.indexOf("{"), strString.indexOf("}")));
                case 'G':
                    return checkAlphaNumeric(strString.substring(strString.indexOf("{"), strString.indexOf("}")));
                case 'E':
                    return checkAlphaNumeric(strString.substring(strString.indexOf("{"), strString.indexOf("}")));
                case 'X':
                    return checkAlphaNumeric(strString.substring(strString.indexOf("{"), strString.indexOf("}")));
                case 'S':
                    return checkAlphaNumeric(strString.substring(strString.indexOf("{"), strString.indexOf("}")));
                default:
                    return false;
            }
        } else {
            return checkAlphaNumeric(strString);
        }
    }
    private static Pattern ALPHANUMERIC = Pattern.compile("[A-Za-z0-9]+");
    private static Pattern ALPHA = Pattern.compile("[A-Za-z]+");
    private static Pattern NUMERIC = Pattern.compile("[0-9]+");

    public boolean checkName(String text) {
        String illegal = "~!@#$%^&*()_+={}[]:;\"|/<>,0123456789";
        if (text == null || text.equals("")) {
            return false;
        }
        for (int i = 0; i < text.length(); i++) {
            for (int x = 0; x < illegal.length(); x++) {
                if (text.charAt(i) == illegal.charAt(x)) {
                    return false;
                }
            }
        }
        return true;
    }

    boolean checkDate(String text) {
        int day = 0;
        int month = 0;
        int year = 0;
        if (text.length() > 10 || text.length() < 6) {
            return false;
        }
        switch (text.length()) {
            case 10:
                month = Integer.parseInt(text.substring(0, 2));
                day = Integer.parseInt(text.substring(3, 5));
                year = Integer.parseInt(text.substring(6, 10));
                break;

            case 8:
                if (checkNumeric(text.substring(2, 3))) {
                    month = Integer.parseInt(text.substring(0, 2));
                    day = Integer.parseInt(text.substring(2, 4));
                    year = Integer.parseInt(text.substring(4, 8));
                } else {
                    month = Integer.parseInt(text.substring(0, 2));
                    day = Integer.parseInt(text.substring(3, 5));
                    year = Integer.parseInt(text.substring(6, 8));
                }
                break;

            case 6:
                month = Integer.parseInt(text.substring(0, 2));
                day = Integer.parseInt(text.substring(2, 4));
                year = Integer.parseInt(text.substring(4, 6));
                break;
        }
        if (month < 1 || month > 12) {
            return false;
        }
        switch (month) {
            case 1:
                if (day < 1 || day > 31) {
                    return false;
                }
                break;
            case 2:
                if (day > 1) {
                    System.out.println(day % 4);
                    if ((day % 4) == 0) {
                        if (day > 29) {
                            return false;
                        } else if (day > 28) {
                            return false;
                        }
                    }
                } else {
                    return false;
                }
                break;
            case 3:
                if (day < 1 || day > 31) {
                    return false;
                }
                break;
            case 4:
                if (day < 1 || day > 30) {
                    return false;
                }
                break;
            case 5:
                if (day < 1 || day > 31) {
                    return false;
                }
                break;
            case 6:
                if (day < 1 || day > 30) {
                    return false;
                }
                break;
            case 7:
                if (day < 1 || day > 31) {
                    return false;
                }
                break;
            case 8:
                if (day < 1 || day > 31) {
                    return false;
                }
                break;
            case 9:
                if (day < 1 || day > 30) {
                    return false;
                }
                break;
            case 10:
                if (day < 1 || day > 31) {
                    return false;
                }
                break;
            case 11:
                if (day < 1 || day > 30) {
                    return false;
                }
                break;
            case 12:
                if (day < 1 || day > 31) {
                    return false;
                }
                break;
        }
        if (year < 75) {
            year += 2000;
        } else if (year > 74 && year < 99) {
            year += 1900;
        }
        return true;
    }

    public boolean checkNotRequired(String text) {
        String illegal = "~!@#$%^&*()_+={}[]:;\"|/<>,0123456789";

        for (int i = 0; i < text.length(); i++) {
            for (int x = 0; x < illegal.length(); x++) {
                if (text.charAt(i) == illegal.charAt(x)) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean checkZIP(String text) {
        if (text.length() < 5 || text.length() > 5) {
            return false;
        }
        if (!checkNumeric(text)) {
            return false;
        }
        return true;
    }

    public boolean checkSSN(String text) {
        if (text.length() < 9 || text.length() > 11 || text.length() == 10) {
            return false;
        }
        if (text.length() == 9) {
            if (!checkNumeric(text)) {
                return false;
            } else {
                if (!checkNumeric(text.substring(0, 3)) && !checkNumeric(text.substring(4, 6)) && !checkNumeric(text.substring(7, 11))) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean checkTextArea(String text) {
        String illegal = "~!@#$%&()+=:;<>";
        if (text == null || text.equals("")) {
            return false;
        }
        for (int i = 0; i < text.length(); i++) {
            for (int x = 0; x < illegal.length(); x++) {
                if (text.charAt(i) == illegal.charAt(x)) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean checkPhone(String text) {
        if (text.length() < 10 || text.length() > 15) {
            return false;
        }
        if (text.length() == 10) {
            if(!checkNumeric(text)) {
                return false;
            }
        }
        else {
            // others
        }
        return true;
    }
}
