/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package childcareexpress;

/**
 *
 * @author Joshua Cain
 */
public class Person {
    private int PersonId;
    private String Firstname;
    private String Lastname;
    private String SSN;
    private String DOB;
    private String Driverslic;
    private int AddrID;
    private AddressEntry myAddress = new AddressEntry();
    private int EmployID;
    private Employer employee = new Employer();

    public Person() {

    }

    /**
     * @return the PersonId
     */
    public int getPersonId() {
        return PersonId;
    }

    /**
     * @param PersonId the PersonId to set
     */
    public void setPersonId(int PersonId) {
        this.PersonId = PersonId;
    }

    /**
     * @return the First name
     */
    public String getFirstname() {
        return Firstname;
    }

    /**
     * @param Firstname the First name to set
     */
    public void setFirstname(String Firstname) {
        this.Firstname = Firstname;
    }

    /**
     * @return the Last name
     */
    public String getLastname() {
        return Lastname;
    }

    /**
     * @param Lastname the Last name to set
     */
    public void setLastname(String Lastname) {
        this.Lastname = Lastname;
    }

    /**
     * @return the SSN
     */
    public String getSSN() {
        return SSN;
    }

    /**
     * @param SSN the SSN to set
     */
    public void setSSN(String SSN) {
        this.SSN = SSN;
    }

    /**
     * @return the DOB
     */
    public String getDOB() {
        return DOB;
    }

    /**
     * @param DOB the DOB to set
     */
    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    /**
     * @return the Driverslic
     */
    public String getDriverslic() {
        return Driverslic;
    }

    /**
     * @param Driverslic the Driverslic to set
     */
    public void setDriverslic(String Driverslic) {
        this.Driverslic = Driverslic;
    }

    /**
     * @return the AddrID
     */
    public int getAddrID() {
        return AddrID;
    }

    /**
     * @param AddrID the AddrID to set
     */
    public void setAddrID(int AddrID) {
        this.AddrID = AddrID;
    }

    /**
     * @return the myAddress
     */
    public AddressEntry getMyAddress() {
        return myAddress;
    }

    /**
     * @param myAddress the myAddress to set
     */
    public void setMyAddress(AddressEntry myAddress) {
        this.myAddress = myAddress;
    }

    /**
     * @return the EmployID
     */
    public int getEmployID() {
        return EmployID;
    }

    /**
     * @param EmployID the EmployID to set
     */
    public void setEmployID(int EmployID) {
        this.EmployID = EmployID;
    }

    /**
     * @return the employee
     */
    public Employer getEmployee() {
        return employee;
    }

    /**
     * @param employee the employee to set
     */
    public void setEmployee(Employer employee) {
        this.employee = employee;
    }
    public String toString() {
        return (this.getLastname() + ", " + this.getFirstname() + " Home Phone: " + this.getMyAddress().getStrHphone());
    }
}
