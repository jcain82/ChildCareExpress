/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package childcareexpress;

/**
 *
 * @author Joshua Cain
 */
public class MedicalContact {
    private int intMedConId;
    private String Name;
    private String Phone;
    private int AddrId;
    private AddressEntry address;

    public MedicalContact() {
        address = new AddressEntry();
    }
    /**
     * @return the intMedConId
     */
    public int getIntMedConId() {
        return intMedConId;
    }

    /**
     * @param intMedConId the intMedConId to set
     */
    public void setIntMedConId(int intMedConId) {
        this.intMedConId = intMedConId;
    }

    /**
     * @return the Name
     */
    public String getName() {
        return Name;
    }

    /**
     * @param Name the Name to set
     */
    public void setName(String Name) {
        this.Name = Name;
    }

    /**
     * @return the Phone
     */
    public String getPhone() {
        return Phone;
    }

    /**
     * @param Phone the Phone to set
     */
    public void setPhone(String Phone) {
        this.Phone = Phone;
    }

    /**
     * @return the AddrId
     */
    public int getAddrId() {
        return AddrId;
    }

    /**
     * @param AddrId the AddrId to set
     */
    public void setAddrId(int AddrId) {
        this.AddrId = AddrId;
        this.address.setIntAddressId(AddrId);
    }

    /**
     * @return the address
     */
    public AddressEntry getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(AddressEntry address) {
        this.address = address;
    }

}
