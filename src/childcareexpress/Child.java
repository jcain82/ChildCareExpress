/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package childcareexpress;

import com.mysql.jdbc.PreparedStatement;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Joshua Cain
 */
public class Child {

    //Child's ID in Database
    int ChildID;

    //Name Information
    String Firstname;
    String Middlename;
    String Lastname;

    //Date of Birth
    String DOB;
    char Sex; // M or F

    //Date of Enrollment
    String Enrolled;

    //Authorization to Transport Child in Emergency situation
    char Transport; // Y or N

    //Medical Information
    char Allergy;
    String AllergyNotes;
    char MedCondition;
    String MedConditionNotes;
    int MedConId;
    MedicalContact MedCon;

    String toDay;
    long Checkin;
    long Checkout;

    Relationship myRelation = new Relationship();
    public static final String DATE_FORMAT_NOW = "yyyy-MM-dd";

    public Child() {
        ChildID = -1;
        Firstname = "";
        Middlename = "";
        Lastname = "";
        Sex = ' '; // M or F
        Enrolled = now();
        Transport = 'N'; // Y or
        MedCon = new MedicalContact();
        toDay = now();
        Checkin = 0;
        Checkout = 0;

    }

    public int StoreNewChild() {
        try {
            databaseMgr dbcon = new databaseMgr();
            this.ChildID = dbcon.AddChild(this);
        } catch (Exception ex) {
            Logger.getLogger(Child.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ChildID;
    }
    //Get Today's Date

    public static String now() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
        return sdf.format(cal.getTime());
    }

    public static String formatDate(String aDate) {
        return aDate;
    }

    public String toString() {
        return (Lastname + ", " + Firstname + " ID:" + ChildID);
    }
}
