/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package childcareexpress;

/**
 *
 * @author Joshua Cain
 */
public class Employee {

    //User Credentials
    private int intID = -1;
    private String UserID = "";
    private String Password = "";
    private boolean admin = false;
    private String strFullName="";
    private boolean validUser = false;
    
    public Employee() {
        validUser = false;
    }

    public Employee(String uid, String pass) {
        UserID = uid;
        Password = pass;
        validUser = false;
    }
   
    public String getUserID() {
        return UserID;
    }

    public void setUserID(String uid) {
        UserID = uid;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String pass) {
        Password = pass;
    }

    public boolean getValidUser() {
        return validUser;
    }

   public void setValidUser(boolean validate) {
        validUser = validate;
    }

   public String getFullName() {
       return strFullName;
   }

   public void setFullName(String FullName) {
       strFullName = FullName;
   }

   public Boolean getAdmin() {
       return admin;
   }

   public void setAdmin(Boolean adm) {
       admin = adm;
   }
}
